\section{Adaptation file overview}

\begin{lstfloat}
\lstset{language=XML}
\begin{lstlisting}
<margot>

	<block name="kernel1">
		<!-- The configuration of block kernel1: -->
		<!--    - Description of the Monitor element -->
		<!--    - Description of the Plan element -->
	</block>
	
	<block name="kernel2">
		<!-- The configuration of block kernel2: -->
		<!--    - Description of the Monitor element -->
		<!--    - Description of the Plan element -->
	</block>

</margot>
\end{lstlisting}
\caption{Minimal adaptation configuration file.}
\label{code:min_adaptation}
\end{lstfloat}

This section describes the adaptation XML files, which define the Monitor and Plan elements for every block of code that will be tuned by mARGOt.
The root element of the XML configuration file is the \textit{margot} tag, which holds the definition of all the block of code of the application.
The configuration of each block of code is inserted in the \textit{block} tag, which has also the attribute \textit{name} which holds the name of the block of code.
The name of the block must be a valid C++ namespace identifier.
\prettyref{code:min_adaptation} shows a simple example on how it is possible to generate an empty configuration for the blocks depicted in \prettyref{fig:code_example1}.

Since each block of code is independent from each other, in the remainder of the documentation we focus only in a single block of code, without losing generality.


\subsection{Describing the monitor element}
\label{ssec:monitor_element}

The description of the Monitor elements consists in the list of Monitors that observe a metric of interest for the developer.
The mARGOt framework ships with a suite of monitors for the most common metrics.
However, the configuration file is able to handle custom monitor as well.


\begin{lstfloat}
\lstset{language=XML}
\begin{lstlisting}
<monitor name="my_custom_monitor" type="custom">

	<spec>
		<header reference="my_monitor.hpp" />
		<class name="MyCustomMonitor" />
		<type name="double" />
		<stop_method name="my_stop" />
		<start_method name="my_start" />
	</spec>
	
	<creation>
		<param name="window_size">
			<fixed value="1" />
		</param>
	</creation>
	
	<start>
		<param>
			<local_var name="start_param" type="int" />
		</param>
	</start>

	<stop>
		<param name="error">
			<local_var name="error" type="double" />
		</param>
	</stop>

	<expose var_name="avg_quality" what="average" />

</monitor>
\end{lstlisting}
\caption{Example of the definition of a monitor element.}
\label{code:monitor_xml}
\end{lstfloat}


In particular, \prettyref{code:monitor_xml} shows an example of the full declaration of a monitor that observe a metric of interest.
Each tag \textit{monitor} states the characteristic of the monitor of interested and it must specify the identifier of the monitor (with the attribute \textit{name}) and its type (with the attribute \textit{type}).
The identifier of the monitor must be a valid C++ identifier.
The type of the monitor is an enumeration of all the available monitor in mARGOt, plus the \textit{custom} enumerator which represents a user-defined monitor.


In current implementation, the available monitor in mARGOt are:
\begin{enumerate}
	\item[Frequency] For the frequency monitor
	\item[Memory] For the memory monitor
	\item[PAPI] For the PAPI monitor
	\item[CPUPROCESS] For the process CPU usage monitor
	\item[CPUSYSTEM] For the system-wide CPU usage monitor
	\item[Temperature] For the temperature monitor
	\item[Throughput] For the throughput monitor
	\item[Time] For the time monitor
	\item[Collector] For the ETHz monitoring framework
	\item[ENERGY] For the energy monitor, using RAPL
	\item[ODROID\_POWER] For the power monitor on Odroid
	\item[ODROID\_ENERGY] For the energy monitor on Odroid
\end{enumerate}


The type of the monitor is not case sensitive.
If the type of the monitor is \textit{custom}, it means that the developer is planning to use a monitor which is not shipped with the framework.
For this reason it should specify also the monitor specification from a C++ point of view.
The latter is defined within the XML tag \textit{spec}.
In particular, the developer should specify:
\begin{enumerate}
	\item The header of the C++ which define the monitor object, using the \textit{reference} attribute of the XML tag \textit{header}
	\item The name of the C++ class, using the \textit{name} attribute of the XML element \textit{class}
	\item The type of the observed values stored in the monitor, using the \textit{name} attribute of the XML tag \textit{type}
	\item If any, the name of the method that starts a measure, using the attribute \textit{name} of the XML tag \textit{start\_method}
	\item If any, the name of the method that stops a measure, using the attribute \textit{name} of the XML tag \textit{stop\_method}
\end{enumerate}

The remainder of the specification for the monitor have two purposes: defining the \textbf{input} and \textbf{output} variables for the monitor.
In particular, the XML tag \textit{creation} states all the parameters of the C++ constructor of the monitor.
The XML tag \textit{start} states all the parameters of the C++ method that starts a measure.
Finally, the XML tag \textit{stop} states all the parameters of the C++ method that stops a measure.

Syntactically, all the parameters are specified by the XML tag \textit{param}, however its actual content depends on the nature of the parameter.
In particular, if the parameter is an immediate value, then it is represented by the attribute \textit{value} of the XML tag \textit{fixed}.
If the parameter must be an l-value or it is not known at compile time, it is possible to forward its value to the developer, relying on function parameters exposed to the developer.
In this case the developer must specify the XML tag \textit{local\_var}, which is defined by the C++ type of the parameter (using the attribute \textit{type}) and the name of the parameter (using the attribute \textit{name}).
Semantically, the parameters depends on the monitor, see \prettyref{appendix:monitor_implementation} for a complete list of parameter for each monitor shipped with mARGOt.

Since each monitor collects the observations in a circular buffer, the output variables for the monitor are the statistical properties of interest for the developer (i.e. the average).
The XML tag \textit{expose} identifies such property.
In particular the attribute \textit{name} states the name of the variable that holds such value (therefore it must be a valid C++ identifier), while the attribute \textit{what} identify the target statistical property.
The following is the list of the available statistical properties:
\begin{enumerate}
	\item[AVERAGE] To retrieve the mean value of the observations
	\item[VARIANCE] To retrieve the variance of the observations
	\item[MAX] To retrieve the maximum value of the observations
	\item[MIN] To retrieve the minimum value of the observations
\end{enumerate}



\subsection{Describing the plan element}

The main goal of the plan element to state the concept of ``best'' for the application developer.
Internally, mARGOt represent such definition as a constrained multi-objective optimization problem.
Since the application might have different definitions of ``best'', according to its evolution or external events, it is possible to state more than one optimization problem (or states in mARGOt context). 



\begin{lstfloat}
\lstset{language=XML}
\begin{lstlisting}
<!-- GOAL SECTION -->
<goal name="exectime_g" monitor="exectime_m" dFun="Average" cFun="GT" value="2" />
<goal name="threads_g" knob_name="num_threads" cFun="LT" value="2" />
<goal name="quality_g" metric_name="quality" cFun="LT" value="80" />

<!-- SW-KNOB SECTION -->
<knob name="num_threads" var_name="threads" var_type="int"/>
<knob name="num_trials" var_name="trials" var_type="int"/>

<!-- OPTIMIZATION SECTION -->
<state name="my_optimization_1" starting="yes" >
	<minimize combination="linear">
		<knob name="num_threads" coef="1.0"/>
		<metric name="exectime" coef="2.0"/>
	</minimize>
	<subject to="quality_g" priority="30" />
</state>

<state name="my_optimization_2" >
	<maximize combination="geometric">
		<metric name="quality" coef="1.0"/>
	</maximize>
	<subject to="threads_g" priority="10" />
	<subject to="exectime_g" metric_name="exec_time" priority="20" />
</state>

\end{lstlisting}
\caption{Example of the definition of the plan element.}
\label{code:plan_xml}
\end{lstfloat}

\prettyref{code:plan_xml} shows an example of a definition of a plan element.
In particular, it includes the definition of three sections: the goal section, the software-knob section and the optimization section.
These three sections are related between each other, the monitors stated in the definition of the monitor element and the application knowledge.
The remainder of this section explains in details such relations.

\subsubsection{Goal section}
\label{ssec:goal}

This section states all the goals that the developer would like to express.
Each goal represents a target that must be reached (i.e. \textit{$<subject>$} must be \textit{$<comparison_function>$} than \textit{$<value>$})
The mere definition of a goal does not influence the selection of the configuration.

A goal is represented by the XML tag \textit{goal}.
The attribute \textit{name} identify the goal and it has to be a valid C++ identifier.
The attribute \textit{value} states the actual numeric value of the goal.
The attribute \textit{cFun} states the comparison function of the goal.
The available comparison functions are:
\begin{enumerate}
	\item[GT] For the ``greater than'' comparison function.
	\item[GE] For the ``greater or equal than'' comparison function.
	\item[LT] For the ``less than'' comparison function.
	\item[LE] For the ``less or equal than'' comparison function. 
\end{enumerate}

There are two types of \textit{$<subject>$} for a goal.
If the goal subject is a metric observed by a monitor, than the goal must define also the attribute \textit{monitor} to identify the target monitor (must be the \textit{name} of a previously defined monitor) and the attribute \textit{dFun} to select which statistical property is the subject of the goal.
The available statistical properties are listed in \prettyref{ssec:monitor_element}.

If the goal subject is a field (either a metric or a software-knob) of the application knowledge which is not monitored at run-time, then the goal must identify the target field.
In particular, if the goal targets a metric, it must define the attribute \textit{metric\_name}, which holds the name of a metric in the related Operating Point (see \prettyref{sec:knowledge}).
If the goal targets a software-knob, it must define the attribute \textit{knob\_name}, which holds the name of a software-knob in the related Operating Point.


\subsubsection{Software-knob section}

This section relates the software-knob states in the application knowledge, with C++ variables that must be exported to the application in the update function.
In particular, each software knob is represented by the XML tag \textit{knob} and must define:
\begin{itemize}
	\item the attribute \textit{name}, which relates to the name of a software knob in the application knowledge
	\item the attribute \textit{var\_name}, which is the name of the exported variable, therefore it must be a valid C++ identifier.
	\item the attribute \textit{var\_type}, states the C++ type of the exported variable.
\end{itemize}

These information are used to compose the signature of the update function, exposed to the user.


\subsubsection{Optimization section}

This section states the list of states,which is the list of constrained multi-objective optimization problems.
In particular, each state is represented by the XML tag \textit{state}, using the attribute \textit{name} to uniquely identify it.
If the developer specify more than one state, it also have to specify which is the starting state, using the attribute \textit{starting} and setting its value to ``yes''.


Each state is composed by an objective function that must be maximized or minimized and the list of constraints.
The objective function is represented by either the XML tag \textit{maximize} or \textit{minimize}, according to the requirements of the application.
Since the objective function is a composition of fields (thus metrics and/or knobs), the attribute \textit{combination} specify the composition formula.
In the current implementation are available two kind of compositions:
\begin{enumerate}
	\item a geometrical combination, represented by the value ``geometric''
	\item a linear combination, represented by the value ``linear''
\end{enumerate}
As child  of the objective function tag, there is the list of fields that compose the objective function.
In particular, the XML tag \textit{metric} represents a metric of the application knowledge, specified by the attribute \textit{name}; while the XML tag \textit{knob} represents a software-knob of the application knowledge, specified by the attribute \textit{name}.
Both cases require to define the attribute \textit{coefficient}, which states the numeric value of the coefficient of each term in the combination.


The list of constraints is represented by the list of XML tags \textit{subject}, using the attribute \textit{to} to specify the related goal name (see \prettyref{ssec:goal}) and the attribute \textit{priority} to specify the priority of constraint.
The priority is a number used to order by importance the constraints, therefore it is required that each constraint of a state must have a different priority.
If the constraints relates to a goal whose subject is a monitored value, it is required to specify the name of the related metric in the application knowledge, using the attribute \textit{metric\_name}





















