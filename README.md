# mARGOt Hell documentation

This repository contains a latex documentation that describes in details the syntax and semantic of the XML configuration files, required to build the high level interface for the mARGOt autotuner.

To build the documentation, it is required to have a valid latex toolchain and the latexmk build system, then just issue the make command in the root directory of the repository.
